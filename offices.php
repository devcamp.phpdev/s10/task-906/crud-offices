<?php
$dbServer = "localhost";
$dbName = "shop_pizza365";
$dbUserName = 'root';
$dbPassword = '';
$conn = new PDO( "mysql:host={$dbServer};dbname={$dbName}", $dbUserName, $dbPassword);
class offices{
    public function __construct(){
        switch ($_SERVER["REQUEST_METHOD"]){
            // Xử lý request method GET
            case "GET": 
                if (isset($_GET["id"])){
                    // trả về thông tin của 1 người dùng
                    $this->getOfficeById($_GET["id"]);
                } else {
                    $this->getAllOffices();
                }
                break;
            // xử lý request method POST
            case "POST":
                $this->insertOffice();
                break;
            // xử lý request method PUT
            case "PUT": 
                $this->updateOffice();
                break;
            // xử lý request method Delete
            case "DELETE":
                $this->deleteOffice();
                break;
        }
    }
    public function insertOffice(){
        global $conn;
        $sql = "INSERT INTO offices (city, phone, address_line, state, country, territory) VALUES (?,?,?,?,?,?)";
        $stmt= $conn->prepare($sql);
        $stmt->execute([$_POST["city"], $_POST["phone"], $_POST["address_line"], $_POST["state"], $_POST["country"], $_POST["territory"]]);
        $this->returnJson(json_encode(["Inserted ID", $conn->lastInsertId()]));
    }

    public function getAllOffices(){
        global $conn;
        $stmt = $conn->query("SELECT * FROM offices");
        $rows = [];
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $rows [] = $row;
        }
        $this->returnJson(json_encode($rows));
    }

    public function getOfficeById(){
        global $conn;
        $id = $_GET["id"];
        $results = $conn->query("SELECT * FROM offices where id = {$id}");
        if ($results && $results->rowCount() == 1 ){
            $rows = $results->fetch(PDO::FETCH_ASSOC);
            $this->returnJson(json_encode($rows));
        } else {
            $this->returnJson(json_encode(["Error", "Not found"]), 404);
        }
        
    }

    public function updateOffice(){
        global $conn;
        $requestBody = file_get_contents('php://input');
        $requestArray = json_decode($requestBody, true);
        $sql = "update offices set city = ?, phone = ?, address_line = ?, state = ?, country = ?, territory = ? where id = ?";
        $stmt= $conn->prepare($sql);
        $stmt->execute([$requestArray["city"], $requestArray["phone"], $requestArray["address_line"], $requestArray["state"], $requestArray["country"], $requestArray["territory"], $requestArray["id"]]);
        $this->returnJson(json_encode(["updated Office ID",$requestArray["id"]]));
    }

    public function deleteOffice(){
        global $conn;
        $sql = "delete from offices where id = ?";
        $stmt= $conn->prepare($sql);
        $stmt->execute([$_GET["id"]]);
        $this->returnJson(json_encode(["deleted Office ID",$_GET["id"]]));
    }

    public function returnJson($jsonString, $responseCode = 200){
        header('Content-Type: application/json'); // thiết lập content type là application/json
        http_response_code($responseCode); // Thiết lập mã trạng thái trả về
        echo $jsonString;   // trả về chuỗi JSON
        die();//Kết thúc chương trình
    }
}

$office = new offices();
